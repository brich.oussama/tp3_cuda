#include <iostream>
#include <ctime>
#include <cstdlib>
 using namespace std;
 __global__ void add(int * a, int * b, int * c, int M){
 	int index = threadIdx.x + blockIdx.x * M;
 	c[index] = a[index] + b[index];
 }

 int main(void){
 int N = 20;
 int M = 64;
 	// get a random array 
 	const int size = 1280;
	 //cout<< "how big do you want the array?" << endl;
	 //cin >> size;



 int host_array_first[size];
 int host_array_second[size];
 int host_array_result[size];

 srand((unsigned)time(0)); 
     
	for(int i=0; i<size; i++){ 
        host_array_first[i] = (rand()%100)+1; 
       // cout << a[i] << "--" << endl;
	 }


   for(int i=0; i<size; i++){
        host_array_second[i] = (rand()%100)+1;
       // cout << b[i] << "--" << endl;
   }


	// Instanciate arrays to null pointer
    int* device_array_first = nullptr;
	int* device_array_second = nullptr;
	int* device_array_result = nullptr;

	// Allocate memory in device
	cudaMalloc((void**)&device_array_result, size * sizeof(int));
	cudaMalloc((void**)&device_array_first, size * sizeof(int));
	cudaMalloc((void**)&device_array_second, size * sizeof(int));


	// Copyin data to device
    cudaMemcpy(device_array_first, host_array_first, size * sizeof(int), cudaMemcpyHostToDevice);    
	cudaMemcpy(device_array_second, host_array_second, size * sizeof(int), cudaMemcpyHostToDevice);
	
	

	// Device function
	add<<<N,M>>>(device_array_first,device_array_second,device_array_result,M);
	
	// Copy output result from device
	cudaMemcpy(host_array_result, device_array_result, size * sizeof(int), cudaMemcpyDeviceToHost);
	
	// Free the device memory
	cudaFree(device_array_result);
	cudaFree(device_array_first);
	cudaFree(device_array_second);
	
	// Display the two array and their add
	cout << "SOMME : " << endl;
	for(int i=0; i<size; i++){
	cout<< i << "  --->  " << host_array_first[i] << " + " << host_array_second[i] << " = " << host_array_result[i] << endl;
	}

 }

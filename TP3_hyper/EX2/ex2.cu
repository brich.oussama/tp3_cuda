#include <iostream>
#include <ctime>
#include <cstdlib>
 using namespace std;
 __global__ void add(int** a, int** b, int** c, int M){
 	int index1 = threadIdx.x + blockIdx.x * M;
 	int index2 = threadIdx.y + blockIdx.y * M;
	//for(int i = 0; i < 32 ;i++){
	 //int index1 = blockIdx.x * blockDim.x + threadIdx.x;
	 //int index2 = blockIdx.y * blockDim.y + threadIdx.y;
		     
	if (index1 < 10 && index2 < 10)
	c[index1][index2] = a[index1][index2] + b[index1][index2];
	//}
 }
 
/* void print_matrix(int matrix[10][10], int rows, int cols){
 for(int i=0; i < rows; ++i){
 for (int j=0; j < cols; ++j){
 cout << matrix[i][j] << "  ";
 }
 cout << endl;
 }
 }
*/
 int main(void){
 int N = 8;
 int M = 32;
 	// get a random array 
 	const int size = 10;
	const int size_matrix = size * size;
	 //cout<< "how big do you want the array?" << endl;
	 //cin >> size;



 int host_array_first[size][size];
 int host_array_second[size][size];
 int host_array_result[size][size];

 srand((unsigned)time(0)); 
     
	for(int i=0; i<size; i++){ 
		for(int j=0;j<size; j++){
	        host_array_first[i][j] = (rand()%100)+1; 
		}
	 }


   for(int i=0; i<size; i++){
		for(int j=0;j<size; j++){
        host_array_second[i][j] = (rand()%100)+1;
		}
	   }


	// Instanciate arrays to null pointer
    int** device_array_first = nullptr;
	int** device_array_second = nullptr;
	int** device_array_result  = nullptr;

	// Allocate GPU buffers for three vectors (two input, one output)
	cudaMalloc((void***)&device_array_result, size_matrix * sizeof(int));
	cudaMalloc((void***)&device_array_first, size_matrix * sizeof(int));
	cudaMalloc((void***)&device_array_second, size_matrix * sizeof(int));


	// Copy input vectors from host memory to GPU buffers.
    cudaMemcpy(device_array_first, host_array_first, size_matrix * sizeof(int), cudaMemcpyHostToDevice);    
	cudaMemcpy(device_array_second, host_array_second, size_matrix * sizeof(int), cudaMemcpyHostToDevice);
	
	

	// Device function
		
	add<<<N,M>>>(device_array_first,device_array_second,device_array_result,M);
	
	// Copy output vector from GPU buffer to host memory.
	cudaMemcpy(host_array_result, device_array_result, size_matrix * sizeof(int), cudaMemcpyDeviceToHost);
	
	// make the host block until the device is finished with foo
	cudaDeviceSynchronize();

	// check for error
	cudaError_t error = cudaGetLastError();
	if(error != cudaSuccess)
	{
	// print the CUDA error message and exit
	printf("CUDA error: %s\n", cudaGetErrorString(error));
	exit(-1);
	}

	// Free the device memory
	cudaFree(device_array_result);
	cudaFree(device_array_first);
	cudaFree(device_array_second);
	
	// Display the two array and their add

	//print_matrix(host_array_first, size,size);
	cout << "" << endl;
	cout << "" << endl;
	cout << "" << endl;
	//print_matrix(host_array_second, size,size);
	cout << "" << endl;
	cout << "" << endl;
	cout << "" << endl;
	cout << "SOMME : " << endl; 
	//print_matrix(host_array_result, size,size);
	for(int i=0; i<size; i++){
	for(int j=0;j<size; j++){
		cout << i << "  --->  " << host_array_first[i][j] << " + " << host_array_second[i][j] << " = " << host_array_result[i][j] << endl ;
		}
	}

 }

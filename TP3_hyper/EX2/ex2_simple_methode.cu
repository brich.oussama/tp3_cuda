#include <iostream>
#include <ctime>
#include <cstdlib>
 using namespace std;
 __global__ void add(int * a, int * b, int * c, int M){
 	int index = threadIdx.x + blockIdx.x * M;
 	c[index] = a[index] + b[index];
 }

 int * matrixToArray(int ** matrix, int array_size){
	int array[array_size];
	int index_array = 0;
	for(int i=0; i<array_size; i++){
		for(int j=0;j<array_size; j++){
			array[index_array] = matrix[i][j];
			index_array++;
		} 
	}
	return array;
 }



 int main(void){
 int N = 20;
 int M = 64;
 	// get a random array 
 	const int size = 1280;
	 //cout<< "how big do you want the array?" << endl;
	 //cin >> size;



 int** host_matrix_first;
 int** host_matrix_second;
int** host_matrix_result;

// generate random matrix
 srand((unsigned)time(0));
     
	for(int i=0; i<size; i++){
		for(int j=0;j<size; j++){
        host_matrix_first[i][j] = (rand()%100)+1; 
	   }
	 }


   for(int i=0; i<size; i++){
       for(int j=0;j<size; j++){
	   host_matrix_second[i][j] = (rand()%100)+1;
       // cout << b[i] << "--" << endl;
		}
   }

	int* host_array_first;
	int* host_array_second;
	
	int* host_array_result;
	// Converting
	host_array_first = matrixToArray(host_matrix_first, (size*size));
	host_array_first = matrixToArray(host_matrix_second, (size*size));

	// Instanciate arrays to null pointer
    int* device_array_first = nullptr;
	int* device_array_second = nullptr;
	int* device_array_result = nullptr;

	
	// Allocate GPU buffers for three vectors (two input, one output)
	cudaMalloc((void**)&device_array_result, size * sizeof(int));
	cudaMalloc((void**)&device_array_first, size * sizeof(int));
	cudaMalloc((void**)&device_array_second, size * sizeof(int));


	// Copy input vectors from host memory to GPU buffers.
    cudaMemcpy(device_array_first, host_array_first, size * sizeof(int), cudaMemcpyHostToDevice);    
	cudaMemcpy(device_array_second, host_array_second, size * sizeof(int), cudaMemcpyHostToDevice);
	
	

	// Device function
	add<<<N,M>>>(device_array_first,device_array_second,device_array_result,M);
	
	// Copy output vector from GPU buffer to host memory.
	cudaMemcpy(host_array_result, device_array_result, size * sizeof(int), cudaMemcpyDeviceToHost);
	
	// Free the device memory
	cudaFree(device_array_result);
	cudaFree(device_array_first);
	cudaFree(device_array_second);
	
	// Display the two array and their add
	cout << "SOMME : " << endl;
	for(int i=0; i<size; i++){
	cout<< i << "  --->  " << host_array_first[i] << " + " << host_array_second[i] << " = " << host_array_result[i] << endl;
	}

 }
